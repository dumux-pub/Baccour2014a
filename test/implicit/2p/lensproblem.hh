// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Soil contamination problem where DNAPL infiltrates a fully
 *        water saturated medium.
 */

#ifndef DUMUX_LENSPROBLEM_HH
#define DUMUX_LENSPROBLEM_HH

#if HAVE_UG
#include <dune/grid/uggrid.hh>
#endif

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/sgrid.hh>

#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/components/dnapl.hh>
#include <dumux/implicit/2p/2pmodel.hh>
#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/cellcentered/ccpropertydefaults.hh>

#include <dumux/material/fluidsystems/h2on2fluidsystem.hh>

#include "lensspatialparams.hh"

namespace Dumux
{

template <class TypeTag>
class LensProblem;

//////////
// Specify the properties for the lens problem
//////////
namespace Properties
{
NEW_TYPE_TAG(LensProblem, INHERITS_FROM(TwoP, LensSpatialParams));
NEW_TYPE_TAG(LensBoxProblem, INHERITS_FROM(BoxModel, LensProblem));
NEW_TYPE_TAG(LensCCProblem, INHERITS_FROM(CCModel, LensProblem));

// Set the grid type
#if HAVE_UG
SET_TYPE_PROP(LensProblem, Grid, Dune::UGGrid<2>);
#else
SET_TYPE_PROP(LensProblem, Grid, Dune::YaspGrid<2>);
#endif

// Set the problem property
SET_TYPE_PROP(LensProblem, Problem, Dumux::LensProblem<TypeTag>);

// TODO: remove this macro switch
#if 1
// Set the wetting phase
SET_PROP(LensProblem, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::SimpleH2O<Scalar> > type;
};

// Set the non-wetting phase
SET_PROP(LensProblem, NonwettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::DNAPL<Scalar> > type;
};
#else
// OR: set the fluid system
SET_TYPE_PROP(LensProblem, FluidSystem, H2ON2FluidSystem<TypeTag>);
#endif

// Enable partial reassembly of the jacobian matrix?
SET_BOOL_PROP(LensProblem, ImplicitEnablePartialReassemble, false);

// Enable reuse of jacobian matrices?
SET_BOOL_PROP(LensProblem, ImplicitEnableJacobianRecycling, false);

// Write the solutions of individual newton iterations?
SET_BOOL_PROP(LensProblem, NewtonWriteConvergence, false);

// Use forward differences instead of central differences
SET_INT_PROP(LensProblem, ImplicitNumericDifferenceMethod, +1);

// Linear solver settings
SET_TYPE_PROP(LensProblem, LinearSolver, Dumux::BoxBiCGStabILU0Solver<TypeTag> );
SET_INT_PROP(LensProblem, LinearSolverVerbosity, 0);
SET_INT_PROP(LensProblem, LinearSolverPreconditionerIterations, 1);
SET_SCALAR_PROP(LensProblem, LinearSolverPreconditionerRelaxation, 1.0);

// Enable gravity
SET_BOOL_PROP(LensProblem, ProblemEnableGravity, false);

NEW_PROP_TAG(BaseProblem);
SET_TYPE_PROP(LensBoxProblem, BaseProblem, ImplicitPorousMediaProblem<TypeTag>);
SET_TYPE_PROP(LensCCProblem, BaseProblem, ImplicitPorousMediaProblem<TypeTag>);

}

/*!
 * \ingroup TwoPModel
 * \ingroup ImplicitTestProblems
 * \brief Soil contamination problem where DNAPL infiltrates a fully
 *        water saturated medium.
 *
 * The domain is sized 6m times 4m and features a rectangular lens
 * with low permeablility which spans from (1 m , 2 m) to (4 m, 3 m)
 * and is surrounded by a medium with higher permability. Note that
 * this problem is discretized using only two dimensions, so from the
 * point of view of the two-phase model, the depth of the domain
 * implicitly is 1 m everywhere.
 *
 * On the top and the bottom of the domain neumann boundary conditions
 * are used, while dirichlet conditions apply on the left and right
 * boundaries.
 *
 * DNAPL is injected at the top boundary from 3m to 4m at a rate of
 * 0.04 kg/(s m^2), the remaining neumann boundaries are no-flow
 * boundaries.
 *
 * The dirichlet boundaries on the left boundary is the hydrostatic
 * pressure scaled by a factor of 1.125, while on the right side it is
 * just the hydrostatic pressure. The DNAPL saturation on both sides
 * is zero.
 *
 * This problem uses the \ref TwoPModel.
 *
 * This problem should typically be simulated until \f$t_{\text{end}}
 * \approx 20\,000\;s\f$ is reached. A good choice for the initial time step
 * size is \f$t_{\text{inital}} = 250\;s\f$.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2p -parameterFile test_box2p.input</tt> or 
 * <tt>./test_cc2p -parameterFile test_cc2p.input</tt>
 */
template <class TypeTag >
class LensProblem : public GET_PROP_TYPE(TypeTag, BaseProblem)
{
    typedef typename GET_PROP_TYPE(TypeTag, BaseProblem) ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, WettingPhase) WettingPhase;
    typedef typename GET_PROP_TYPE(TypeTag, NonwettingPhase) NonwettingPhase;

    enum {

        // primary variable indices
        pwIdx = Indices::pwIdx,
        snIdx = Indices::snIdx,

        // equation indices
        contiNEqIdx = Indices::contiNEqIdx,

        // phase indices
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx,


        // world dimension
        dimWorld = GridView::dimensionworld
    };


    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*!
     * \brief The constructor
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    LensProblem(TimeManager &timeManager,
                const GridView &gridView)
    : ParentType(timeManager, gridView)
    {
        eps_ = 3e-6;
        pnRef_ = 1e5;//Luftdruck
        temperature_ = 273.15 + 20; // -> 20°C

        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, 
                                             std::string, 
                                             Problem, 
                                             Name);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        return name_.c_str();
    }

    /*!
     * \brief User defined output after the time integration
     *
     * Will be called diretly after the time integration.
     */
    void postTimeStep()
    {
        // Calculate storage terms
        PrimaryVariables storage;
        this->model().globalStorage(storage);

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0) {
            std::cout<<"Storage: " << storage << std::endl;
        }
    }

    /*!
     * \brief Returns the temperature \f$ K \f$
     *
     * This problem assumes a uniform temperature of 20 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; };

    /*!
     * \brief Returns the source term
     *
     * \param values Stores the source values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} / (m^\textrm{dim} \cdot s )] \f$
     * \param globalPos The global position
     */
    void sourceAtPos(PrimaryVariables &values,
                const GlobalPosition &globalPos) const
    {
        const Scalar time = this->timeManager().time()+ this->timeManager().timeStepSize();
        const Scalar pwTop = 98942.8;
        const Scalar pwBottom = 95641.1;
        const Scalar globalPos1 = globalPos[1];
        const Scalar time_Var = time;

//******************************************************************************************        
            

            

values[pwIdx]=(globalPos1*globalPos1*globalPos1*globalPos1)*(pwBottom-pwTop)*(-1.0107421875E-7)-globalPos1*(pwBottom-pwTop)*(time_Var*9.199999999999999E-7+(globalPos1*globalPos1*globalPos1)*7.187499999999999E-7-1.84E-4)*(3.0/3.2E1)-4.0/5.0;//Lineares Modell einfach


            /*values[pwIdx]=pow(tanh(globalPos1*5.0-time_Var*(1.0/1.0E1)-5.0),2.0)*1.6E1+(pow(tanh(globalPos1*5.0-time_Var*(1.0/1.0E1)-5.0),2.0)*9.199999999999999E-4-9.199999999999999E-4)*(pow(tanh(globalPos1*5.0+time_Var*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+tanh(globalPos1*5.0+time_Var*(1.0/1.0E1)-1.5E1)*(tanh(globalPos1*5.0-time_Var*(1.0/1.0E1)-5.0)*1.84E-4+2.3E-4)*(pow(tanh(globalPos1*5.0+time_Var*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*1.0E1-1.6E1;//Lineares Modell komplex*/


values[snIdx]=-(globalPos1*9.375E7-globalPos1*(pwBottom-pwTop)*(3.0/3.2E1))*(time_Var*2.356491228070175E-6+(globalPos1*globalPos1*globalPos1)*1.841008771929824E-6+7.069473684210526E-4)+(globalPos1*globalPos1)*((globalPos1*globalPos1)*(pwBottom-pwTop)*(3.0/6.4E1)-(globalPos1*globalPos1)*4.6875E7)*5.523026315789473E-6+1.46E2/1.25E2;// Lineares Modell einfach



            /*values[snIdx]= -(tanh(globalPos1*5.0-time_Var*(1.0/1.0E1)-5.0)*(pow(tanh(globalPos1*5.0-time_Var*(1.0/1.0E1)-5.0),2.0)*5.0-5.0)*4.0E10+tanh(globalPos1*5.0+time_Var*(1.0/1.0E1)-1.5E1)*(pow(tanh(globalPos1*5.0+time_Var*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*1.0E1)*(tanh(globalPos1*5.0-time_Var*(1.0/1.0E1)-5.0)*4.712982456140351E-4-5.891228070175438E-4)-pow(tanh(globalPos1*5.0-time_Var*(1.0/1.0E1)-5.0),2.0)*(5.84E2/2.5E1)-(pow(tanh(globalPos1*5.0-time_Var*(1.0/1.0E1)-5.0),2.0)*2.356491228070175E-3-2.356491228070175E-3)*(pow(tanh(globalPos1*5.0-time_Var*(1.0/1.0E1)-5.0),2.0)*2.0E10+(pow(tanh(globalPos1*5.0+time_Var*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))-2.0E10)+5.84E2/2.5E1;// Lineares Modell komplex*/




    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param values Stores the value of the boundary type
     * \param globalPos The global position
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
            const GlobalPosition &globalPos) const
    {
        //if (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)) {
        if (onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos)){
            values.setAllDirichlet();
        }
        else {
            values.setAllNeumann();
        }
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet
     *        boundary segment
     *
     * \param values Stores the Dirichlet values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variable} ] \f$
     * \param globalPos The global position
     */
    void dirichletAtPos(PrimaryVariables &values,
                        const GlobalPosition &globalPos) const
    {
        //typename GET_PROP_TYPE(TypeTag, FluidState) fluidState;
        //fluidState.setTemperature(temperature_);
        //fluidState.setPressure(FluidSystem::wPhaseIdx, /*pressure=*/1e5);
        //fluidState.setPressure(FluidSystem::nPhaseIdx, /*pressure=*/1e5);

        //Scalar densityW = FluidSystem::density(fluidState, FluidSystem::wPhaseIdx);

        //Scalar height = this->bBoxMax()[1] - this->bBoxMin()[1];
        //Scalar depth = this->bBoxMax()[1] - globalPos[1];
        //Scalar alpha = 1 + 1.5/height;
        //Scalar width = this->bBoxMax()[0] - this->bBoxMin()[0];
        //Scalar factor = (width*alpha + (1.0 - alpha)*globalPos[0])/width;

        // hydrostatic pressure scaled by alpha
        
        const Scalar time = this->timeManager().time()+ this->timeManager().timeStepSize() ;

        Scalar sn = 0.6+0.002*time+globalPos[1]*globalPos[1]*globalPos[1]/640.0; //einfach
        //sn = 2.0/5.0 * (std::tanh( (5.0 * (-globalPos[1]+4)) - 15.0 + time/10.0)) + 1.0/2.0; //komplex
          
        const Scalar pwTop = 98942.8;
        const Scalar pwBottom = 95641.1;
        Scalar pw = pwBottom
          + 0.5 * (std::tanh( (5.0 * globalPos[1]) - 15.0 + time/10.0) + 1.0) * (pwTop - pwBottom);//komplex
        pw = pwBottom + (pwTop - pwBottom)*(globalPos[1]*globalPos[1]*globalPos[1]+time/10.0)*0.25*0.25*0.25;//einfach     
      
        values[snIdx] = sn;    
        values[pwIdx] = pw;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values Stores the Neumann values for the conservation equations in
     *               \f$ [ \textnormal{unit of conserved quantity} / (m^(dim-1) \cdot s )] \f$
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumannAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        values = 0.0;
    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{


    /*!
     * \brief Evaluates the initial values for a control volume
     *
     * \param values Stores the initial values for the conservation equations in
     *               \f$ [ \textnormal{unit of primary variables} ] \f$
     * \param globalPos The global position
     */
    void initialAtPos(PrimaryVariables &values,
                      const GlobalPosition &globalPos) const
    {
        dirichletAtPos(values, globalPos);
    }
    // \}

private:

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->bBoxMax()[1] - eps_;
    }

    Scalar temperature_;
    Scalar eps_;
    Scalar pnRef_;
    std::string name_;
};
} //end namespace

#endif
