// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A water infiltration problem with a low-permeability lens
 *        embedded into a high-permeability domain which uses the
 *        Richards box model.
 */
#ifndef DUMUX_RICHARDS_LENSPROBLEM_HH
#define DUMUX_RICHARDS_LENSPROBLEM_HH

#include <cmath>
#include <dune/geometry/type.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/io/file/dgfparser.hh>

#include <dumux/implicit/richards/richardsmodel.hh>
#include <dumux/material/components/simpleh2o.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>

#include "richardslensspatialparams.hh"

namespace Dumux
{

template <class TypeTag>
class RichardsLensProblem;

//////////
// Specify the properties for the lens problem
//////////
namespace Properties
{
NEW_TYPE_TAG(RichardsLensProblem, INHERITS_FROM(Richards, RichardsLensSpatialParams));
NEW_TYPE_TAG(RichardsLensBoxProblem, INHERITS_FROM(BoxModel, RichardsLensProblem));
NEW_TYPE_TAG(RichardsLensCCProblem, INHERITS_FROM(CCModel, RichardsLensProblem));

// Use 2d YaspGrid
SET_TYPE_PROP(RichardsLensProblem, Grid, Dune::YaspGrid<2>);

// Set the physical problem to be solved
SET_PROP(RichardsLensProblem, Problem)
{ typedef Dumux::RichardsLensProblem<TypeTag> type; };

// Set the wetting phase
SET_PROP(RichardsLensProblem, WettingPhase)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::SimpleH2O<Scalar> > type;
};

// Enable gravity
SET_BOOL_PROP(RichardsLensProblem, ProblemEnableGravity, true);

// Enable partial reassembly of the Jacobian matrix
SET_BOOL_PROP(RichardsLensProblem, ImplicitEnablePartialReassemble, true);


//*************************************************************************************

// Enable re-use of the Jacobian matrix for the first iteration of a time step
SET_BOOL_PROP(RichardsLensProblem, ImplicitEnableJacobianRecycling, true);

// Use forward differences to approximate the Jacobian matrix
SET_INT_PROP(RichardsLensProblem, ImplicitNumericDifferenceMethod, +1);

// Set the maximum number of newton iterations of a time step
SET_INT_PROP(RichardsLensProblem, NewtonMaxSteps, 28);

// Set the "desireable" number of newton iterations of a time step
SET_INT_PROP(RichardsLensProblem, NewtonTargetSteps, 18);

// Do not write the intermediate results of the newton method
SET_BOOL_PROP(RichardsLensProblem, NewtonWriteConvergence, false);
}

/*!
 * \ingroup RichardsModel
 * \ingroup ImplicitTestProblems
 *
 * \brief A water infiltration problem with a low-permeability lens
 *        embedded into a high-permeability domain which uses the
 *        Richards box model.
 *
 * The domain is box shaped. Left and right boundaries are Dirichlet
 * boundaries with fixed water pressure (fixed Saturation \f$S_w = 0\f$),
 * bottom boundary is closed (Neumann 0 boundary), the top boundary
 * (Neumann 0 boundary) is also closed except for infiltration
 * section, where water is infiltrating into an initially unsaturated
 * porous medium. This problem is very similar the the LensProblem
 * which uses the TwoPBoxModel, with the main difference being that
 * the domain is initally fully saturated by gas instead of water and
 * water instead of a %DNAPL infiltrates from the top.
 * 
 * This problem uses the \ref RichardsModel
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_boxrichards -parameterFile test_boxrichards.input -TimeManager.TEnd 10000000</tt>
 * <tt>./test_ccrichards -parameterFile test_ccrichards.input -TimeManager.TEnd 10000000</tt>
 *
 * where the initial time step is 100 seconds, and the end of the
 * simulation time is 10,000,000 seconds (115.7 days)
 */
template <class TypeTag>
class RichardsLensProblem : public RichardsBoxProblem<TypeTag>
{
    typedef RichardsBoxProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::template Codim<0>::Entity::Geometry Geometry;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, MaterialLaw) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // copy some indices for convenience
        pwIdx = Indices::pwIdx,
        contiEqIdx = Indices::contiEqIdx
    };
    enum {
        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*!
     * \brief Constructor
     *
     * \param timeManager The Dumux TimeManager for simulation management.
     * \param gridView The grid view on the spatial domain of the problem
     */
    RichardsLensProblem(TimeManager &timeManager,
                        const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        eps_ = 3e-6;
        pnRef_ = 1e5; // air pressure
        name_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    /*!
     * \brief Returns the temperature [K] within a finite volume
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // -> 10°C

    /*!
     * \brief Returns the reference pressure [Pa] of the non-wetting
     *        fluid phase within a finite volume
     *
     * This problem assumes a constant reference pressure of 1 bar.
     *
     * \param element The DUNE Codim<0> entity which intersects with
     *                the finite volume in question
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The sub control volume index inside the finite
     *               volume geometry
     */
    Scalar referencePressure(const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const int scvIdx) const
    { return pnRef_; }

Scalar dirac(const Scalar x) const
{
return 0;
}

Scalar heaviside(Scalar x) const
{    
    if (x<0)
    return 0;
    else
    return 1;
}

   

//*************************************************************************************************************
    void sourceAtPos(PrimaryVariables &values,
                const GlobalPosition &globalPos) const
    {
        const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();
        const Scalar pwTop = 98942.8;
        const Scalar pwBottom = 95641.1;

    /*values=(globalPos[1]*globalPos[1]*globalPos[1])*(pwBottom-pwTop)*(-6.25E-12)-(globalPos[1]*globalPos[1]*globalPos[1]*globalPos[1])*(time*time)*pow(pwBottom-pwTop,2.0)*1.0986328125E-22+globalPos[1]*time*(pwBottom-pwTop)*(pwBottom*5.0E-16-(globalPos[1]*globalPos[1]*globalPos[1])*time*(pwBottom-pwTop)*7.8125E-20+4.99995E-6)*9.375E-4; //Lineares Modell einfache Lösung*/

    /*values=pow(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0,2.0)*pow(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0),2.0)*(-5.0E-16)+(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*(1.0/1.0E1)-1.0/1.0E1)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*4.0E-8+tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(pwBottom*5.0E-16-(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*5.0E-16+4.99995E-6)*1.0E1; //Lineares Modell komplexe Lösung*/

    /*values=(globalPos[1]*globalPos[1]*globalPos[1])*(pwBottom-pwTop)*1.0/pow(pwBottom*(-1.0/1.0E3)+(globalPos[1]*globalPos[1]*globalPos[1])*time*(pwBottom-pwTop)*1.5625E-7+1.0E2,3.0)*(-1.25E-4)+globalPos[1]*time*(pwBottom-pwTop)*1.0/pow(pwBottom*(-1.0/1.0E3)+(globalPos[1]*globalPos[1]*globalPos[1])*time*(pwBottom-pwTop)*1.5625E-7+1.0E2,8.0)*4.6875E-9-(globalPos[1]*globalPos[1]*globalPos[1]*globalPos[1])*(time*time)*pow(pwBottom-pwTop,2.0)*1.0/pow(pwBottom*(-1.0/1.0E3)+(globalPos[1]*globalPos[1]*globalPos[1])*time*(pwBottom-pwTop)*1.5625E-7+1.0E2,9.0)*8.789062499999999E-15; //Brooks-Corey-Modell einfache Lösung*/

   /*values=pow(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0,2.0)*pow(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0),2.0)*1.0/pow(pwBottom*(-1.0/1.0E3)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(1.0/1.0E3)+1.0E2,9.0)*(-4.0E-8)+(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*(1.0/1.0E1)-1.0/1.0E1)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*1.0/pow(pwBottom*(-1.0/1.0E3)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(1.0/1.0E3)+1.0E2,3.0)*(4.0/5.0)+tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*1.0/pow(pwBottom*(-1.0/1.0E3)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(1.0/1.0E3)+1.0E2,8.0)*5.0E-5; //Brooks-Corey-Modell komplexe Lösung*/
 


    values=(dirac(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*(1.0/1.0E1)-1.0/1.0E1)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))-dirac(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.9E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*(1.0/1.0E1)-1.0/1.0E1)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0)))*1.0/pow(pwBottom*(-1.0/1.0E3)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(1.0/1.0E3)+1.0E2,2.0)*4.0E2+heaviside(pwBottom-(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))-9.9E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*(1.0/1.0E1)-1.0/1.0E1)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*8.0E-4+heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*(1.0/1.0E1)-1.0/1.0E1)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*8.0E-4+dirac(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*(1.0/1.0E1)-1.0/1.0E1)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(pwBottom*(-2.0E-6)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+1.7E1/1.0E2)*4.0E2+dirac(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.9E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*(1.0/1.0E1)-1.0/1.0E1)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(pwBottom*2.0E-6-(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+4.01E2/5.0E2)*4.0E2+tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*pow(-heaviside(pwBottom-(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))-9.9E4)*(pwBottom*2.0E-6-(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+4.01E2/5.0E2)+(heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)-heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.9E4))*1.0/pow(pwBottom*(-1.0/1.0E3)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(1.0/1.0E3)+1.0E2,2.0)+heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)*(pwBottom*(-2.0E-6)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+1.7E1/1.0E2),4.0)*5.0E-5+(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*pow(-heaviside(pwBottom-(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))-9.9E4)*(pwBottom*2.0E-6-(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+4.01E2/5.0E2)+(heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)-heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.9E4))*1.0/pow(pwBottom*(-1.0/1.0E3)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(1.0/1.0E3)+1.0E2,2.0)+heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)*(pwBottom*(-2.0E-6)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+1.7E1/1.0E2),3.0)*((dirac(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))-dirac(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.9E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0)))*1.0/pow(pwBottom*(-1.0/1.0E3)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(1.0/1.0E3)+1.0E2,2.0)+heaviside(pwBottom-(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))-9.9E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+dirac(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(pwBottom*(-2.0E-6)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+1.7E1/1.0E2)+dirac(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.9E4)*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(pwBottom*2.0E-6-(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*2.0E-6+4.01E2/5.0E2)-(heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)-heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.9E4))*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*5.0-5.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*1.0/pow(pwBottom*(-1.0/1.0E3)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(1.0/1.0E3)+1.0E2,3.0)*(1.0/5.0E2))*2.0E-5-(heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.0E4)-heaviside(-pwBottom+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))+9.9E4))*(pow(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1),2.0)*(1.0/1.0E1)-1.0/1.0E1)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*1.0/pow(pwBottom*(-1.0/1.0E3)+(tanh(globalPos[1]*5.0+time*(1.0/1.0E1)-1.5E1)+1.0)*(pwBottom*(1.0/2.0)-pwTop*(1.0/2.0))*(1.0/1.0E3)+1.0E2,3.0)*(4.0/5.0);

    
    }

//*************************************************************************************************************
    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the boundary type is set
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
                       const GlobalPosition &globalPos) const
    {
        if (onLowerBoundary_(globalPos) ||
            onUpperBoundary_(globalPos))
        {
            values.setAllDirichlet();
        }
        else
            values.setAllNeumann();
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the Dirichlet value is set
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values,
                   const GlobalPosition &globalPos) const
    {
        const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize() ;
        const Scalar pwTop = 98942.8;
        const Scalar pwBottom = 95641.1;
        Scalar pw = pwBottom
          + 0.5 * (std::tanh( (5.0 * globalPos[1]) - 15.0 + time/10.0) + 1.0) * (pwTop - pwBottom);//komplex
        //pw = pwBottom + (pwTop - pwBottom) * globalPos[1]*globalPos[1]*globalPos[1]*(time/100.0)* 0.25*0.25*0.25;//einfach
        values[pwIdx] = pw;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * \param values The neumann values for the conservation equations
     * \param globalPos The position for which the Neumann value is set
     */
    void neumannAtPos(PrimaryVariables &values,
                 const GlobalPosition &globalPos) const
    {
        values = 0.0;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial values for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     *
     * \param values Storage for all primary variables of the initial condition
     * \param globalPos The position for which the boundary type is set
     */
    void initialAtPos(PrimaryVariables &values,
                 const GlobalPosition &globalPos) const
    {
        const Scalar time = this->timeManager().time();
        analyticalSolution(values, time, globalPos);
    }

    // \}

    /*!
     * \brief Evaluate the analytical solution.
     *
     * \param values The dirichlet values for the primary variables
     * \param time The time at wich the solution should be evaluated
     * \param globalPos The position for which the Dirichlet value is set
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void analyticalSolution(PrimaryVariables &values,
                   const Scalar time,
                   const GlobalPosition &globalPos) const
    {
        const Scalar pwTop = 98942.8;
        const Scalar pwBottom = 95641.1;
        Scalar pw = pwBottom
          + 0.5 * (std::tanh( (5.0 * globalPos[1]) - 15.0 + time/10.0) + 1.0) * (pwTop - pwBottom);
       // pw = pwBottom + (pwTop - pwBottom) * globalPos[1]*globalPos[1]*globalPos[1]*(time/100.0)* 0.25*0.25*0.25;
        values[pwIdx] = pw;
    }

    /*!
     * \brief Calculate the L2 error between the solution given by
     *        dirichletAtPos and the numerical approximation.
     * \note Works for cell-centered FV only because the numerical
     *       approximation is only evaluated in the cell center (once).
     *       To extend this function to the box method the evaluation
     *       has to be exted to box' subvolumes.
     */
    Scalar calculateL2Error()
    {
        const unsigned int qOrder = 4;
        Scalar l2error = 0.0;
        Scalar l2analytic = 0.0;
        const Scalar time = this->timeManager().time() + this->timeManager().timeStepSize();

        for (auto elementIt = this->gridView().template begin<0>();
             elementIt != this->gridView().template end<0>(); ++elementIt)
        {
            // value from numerical approximation
            Scalar numericalSolution =
                this->model().curSol()[this->model().dofMapper().map(*elementIt, 0, 0)];

            // integrate over element using a quadrature rule
            Geometry geometry = elementIt->geometry();
            Dune::GeometryType gt = geometry.type();
            Dune::QuadratureRule<Scalar, dim> rule =
                Dune::QuadratureRules<Scalar, dim>::rule(gt, qOrder);

            for (typename Dune::QuadratureRule<Scalar, dim>::const_iterator qIt = rule.begin();
                 qIt != rule.end(); ++qIt)
            {
                // evaluate analytical solution
                Dune::FieldVector<Scalar, dim> globalPos = geometry.global(qIt->position());
                Dune::FieldVector<Scalar, 1> values(0.0);
                analyticalSolution(values, time, globalPos);
                // add contributino of current quadrature point
                l2error += (numericalSolution - values[0]) * (numericalSolution - values[0]) * 
                    qIt->weight() * geometry.integrationElement(qIt->position());
                l2analytic += values[0] * values[0] *
                    qIt->weight() * geometry.integrationElement(qIt->position());
            }
        }
        return std::sqrt(l2error/l2analytic);
    }

    /*!
     * \brief Write the relevant secondary variables of the current
     *        solution into an VTK output file.
     */
    void writeOutput(const bool verbose = true)
    {
        ParentType::writeOutput(verbose);

        Scalar l2error = calculateL2Error();

        // compute L2 error if analytical solution is available
        std::cout.precision(8);
        std::cout << "L2 error for "
                  << std::setw(6) << this->gridView().size(0)
                  << " elements: "
                  << std::scientific
                  << l2error
                  << std::endl;
    }

private:

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->bBoxMax()[1] - eps_;
    }

    Scalar eps_;
    Scalar pnRef_;
    std::string name_;
};
} //end namespace

#endif
