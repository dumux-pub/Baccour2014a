Readme
===============================

Content
--------------

The content of this DUNE module was extracted from the module dumux-devel.
In particular, the following subfolders of dumux-devel have been extracted:

- test/implicit/richards,
- test/implicit/2p,

Additionally, all headers in dumux-devel that are required to build the
executables from the sources

- test/implicit/richards/test_boxrichards.cc,
- test/implicit/richards/test_ccrichards.cc,
- test/implicit/2p/test_box2p.cc,
- test/implicit/2p/test_cc2p.cc,

have been extracted.

Installation
============

You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above. For the basic
dependencies see dune-project.org.

The easiest way is to use the `installBaccour2014a.sh` in this folder.
You might want to look at it before execution [here](https://git.iws.uni-stuttgart.de/dumux-pub/Baccour2014a/raw/master/installBaccour2014a.sh). Create a new folder containing the script and execute it.
You can copy the following to a terminal:
```bash
mkdir -p Baccour2014a && Baccour2014a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Baccour2014a/raw/master/installBaccour2014a.sh
chmod +x installBaccour2014a.sh && ./installBaccour2014a.sh
```
It will install this module will take care of most dependencies.
For the basic dependencies see dune-project.org and at the end of this README.

Used Version
===============================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

- dumux...................: version 2.5.0 (/temp/gruenich/dune/dune2.3/dumux)
- dune-common.............: version 2.3.1 (/temp/gruenich/dune/dune2.3/dune-common)
- dune-geometry...........: version 2.3.1 (/temp/gruenich/dune/dune2.3/dune-geometry)
- dune-grid...............: version 2.3.1 (/temp/gruenich/dune/dune2.3/dune-grid)
- dune-istl...............: version 2.3.1 (/temp/gruenich/dune/dune2.3/dune-istl)
- dune-localfunctions.....: version 2.3.1 (/temp/gruenich/dune/dune2.3/dune-localfunctions)
- dune-multidomain........: version 0.1 (/temp/gruenich/dune/dune2.3/dune-multidomain)
- dune-multidomaingrid....: version 2.3.0-rc2 (/temp/gruenich/dune/dune2.3/dune-multidomaingrid)
- dune-pdelab.............: version 1.1.0 (/temp/gruenich/dune/dune2.3/dune-pdelab)
- ALBERTA.................: no
- ALUGrid.................: no
- AmiraMesh...............: no
- BLAS....................: yes
- Eigen...................: yes
- GMP.....................: yes
- Grape...................: no
- METIS...................: yes
- METIS...................: yes
- MPI.....................: no
- OpenGL..................: yes (add GL_LIBS to LDADD manually, etc.)
- PETSc...................: no
- ParMETIS................: no
- SuperLU-DIST............: no
- SuperLU.................: yes (version 4.3 or newer)
- UG......................: no
- UMFPACK.................: no
- psurface................: no
